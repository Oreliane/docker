## Docker pour GV-SERVICES

![logo-baleine-docker.png](/images/0.kz1u7tug1a9.png)

---

![comparatifVM.png](/images/comparatifVM.png)

---
## Image et conteneur
**Image** : un template read-only, utilisé pour créer des conteneurs.

**Conteneur** : Une instance runnable d'une image.

![0.3y5q04nk0v5](/images/0.3y5q04nk0v5.png)

---
### Remplacer la chroot
#### La chroot
- ''*rapidité*'' d'installation d'un nouveau poste
- gestion d'un compte commun pour les connexions aux machines
- Uniformisation de l'environnement de développement

---
### Solution ISO avec Docker

Une seule image avec toutes les versions
![0.zytzc1zbuyi](/images/0.zytzc1zbuyi.png)

/!\ Idea Ultimate ne peut être ouvert 2 fois donc un seul conteneur à la fois

---
### Solution Docker-compose
> **Permet de gérer un ensemble de conteneurs**

![0.th9zqr9h7ga](/images/0.th9zqr9h7ga.png)


---
### Avantages pour nous
- Contrôle de l'environnement de développement
- Uniformisation des postes de développement via image sur git
- Séparation des environnements de test
- Facilité et rapidité pour changer d'environnement

